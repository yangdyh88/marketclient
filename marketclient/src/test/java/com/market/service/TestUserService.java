package com.market.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.market.BaseTest;
import com.market.model.User;

public class TestUserService extends BaseTest {

	@Autowired
	private UserService userService;
	
	@Test
	public void testInsertUser() {
		User user = new User();
		user.setUserName("张明");
		boolean bln = userService.insertUser(user);
		System.out.println(bln);
	}
}
