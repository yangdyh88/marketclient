package com.market.service;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.market.BaseTest;
import com.market.model.Manager;

public class TestManagerService extends BaseTest {
	@Autowired
	private ManagerService managerService;
	
	@Test
	public void testInsert() {
		Manager manager = new Manager();
		manager.setUserCode("10000");
		System.out.println(manager);
		boolean fln = managerService.insert(manager);
		Assert.assertEquals(true, fln);
	}
}
