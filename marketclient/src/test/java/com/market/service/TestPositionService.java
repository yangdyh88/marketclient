package com.market.service;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.market.BaseTest;
import com.market.model.Position;

public class TestPositionService extends BaseTest {
	@Autowired
	private PositionService positionService;
	
	@Test
	public void testInsert() {
		Position p = new Position();
		p.setGoodsName("iphone6");
		System.out.println(p);
		positionService.insert(p);
	}
	
	@Test
	public void testFindPositionById() {
		Position p =positionService.findPositionById(4);
		System.out.println(p);
	}
	
	@Test
	public void testFindPositionList() {
		List<Position> list = positionService.findPositionList();
		System.out.println(list);
	}
}
