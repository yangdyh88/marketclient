package com.market;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/** 
 * @Title: TestBase.java  
 * @Package com.czd
 * @Description: 测试用基类
 * @author Jail    E -Mail:86455@ czd.com
 * @date 2012-12-5 下午3:57:58 
 * @version V1.0 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/applicationContext.xml"})
public class BaseTest {

}
  
    