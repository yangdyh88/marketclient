$(function() {
    $('.pagination').on('click', 'a', function (e) {
        var search = buildSearch({
            pageNo: $(this).data().type
        });
        window.location.href = window.location.pathname + search;
        e.preventDefault();
    });
});
//生成新的查询字符串
function buildSearch(obj){
    var searchArray = [];
    if(location.search.length > 0){
    searchArray = location.search.substr(1).split('&');
    }
    var searchMap = {}, result = [], temp;
    for(var i = 0; i < searchArray.length; i++){
    temp = searchArray[i].split('=');
    searchMap[temp[0]] = temp[1];
    }
    for(i in obj){
    if(obj[i] == ''){
    //delete obj[i];
    }
    }
    $.extend(searchMap, obj);
    for(i in searchMap){
    result.push(i + '=' + searchMap[i]);
    }
    return '?' + result.join('&');
    }