$(function(){
	setInterval(updateNewQuote, 5000); 
	$("#admin_list_tab1").addClass("height","60px").addClass("overflow-y","auto");
});
//更新报价
function updateNewQuote() {
	$.ajax({
		type:"POST",
		url:"/quote",
		timeout:5000,
		success:function(data){
			//平仓价
			var price = data; 
			var profitLoss = 0;
			$("#admin_list_tab1 tbody tr td:nth-child(8)").each(function () {
		        $(this).text(price);
		        
			});
			var $tr1 = $("#admin_list_tab1 tbody tr");
			$tr1.each(function() {
				//买/卖
				var $buyOrSell = $("td:eq(2)",this);
				//持仓价
				var $openPositionPrice = $("td:eq(6)",this);
				//当日浮动盈亏
				var $curProfitLoss = $("td:eq(10)",this);
				var curProfitLoss = 0;
				if($buyOrSell.text().trim().localeCompare("买入") == 0) {
					//买入：当日浮动盈亏=(平仓价-持仓价)*15
					curProfitLoss = (price - $openPositionPrice.text()) * 15;
					$curProfitLoss.text(curProfitLoss);
					if(curProfitLoss >= 0) $curProfitLoss.css("color", "red");
					else $curProfitLoss.css("color", "green");
				}
				else if($buyOrSell.text().trim().localeCompare("卖出") == 0) {
					//卖出：当日浮动盈亏=(持仓价-平仓价)*15
					curProfitLoss = ($openPositionPrice.text() - price) * 15
					$curProfitLoss.text(curProfitLoss);
					if(curProfitLoss >= 0) $curProfitLoss.css("color", "red");
					else $curProfitLoss.css("color", "green");
				}
				
				profitLoss += curProfitLoss;
			});
			
			var $tr2 = $("admin_list_tab2 tbody tr");
			$tr2.each(function() {
				var $profitLoss = $("td:eq(5)",this); //浮动盈亏
				var $primeProfit = $("td:eq(2)",this); //期初权益
				var $curProfit = $("td:eq(3)",this); //当前权益
				
				$profitLoss.text(profitLoss);
				//当前权益=期初权益+浮动盈亏
				$curProfit.text($primeProfit.text() + profitLoss);
				
				if(profitLoss >= 0)
					$profitLoss.css("color", "red");
				else
					$profitLoss.css("color", "green");
			});
		},
		error:function(msg) {
			console.log(msg);
		}
	});
}