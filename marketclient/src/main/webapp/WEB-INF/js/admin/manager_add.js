$(function(){
	$("#addManagerForm").validate({
		rules : {
			userName : 'required',
			userCode :	'required',
			primeProfit : {
				required : true,
				number : true
			},
			money : 'required',
			profitLossTotal : {
				required : true,
				number : true
			},
			canDeposit : {
				required : true,
				number : true
			},
			curFeeTotal : {
				required : true,
				number : true
			},
			occupyDeposit : {
				required : true,
				number : true
			},
			preDelayFee : {
				required : true,
				number : true
			},
			riskRate : 'required',
			freezeDeposit : {
				required : true,
				number : true
			},
			freezeFee : {
				required : true,
				number : true
			}
			
		},
		messages : {
			userName : {
				required : '请输入账户名称',
			},
			userCode :	{
				required : '请输入登录账号',
			},
			primeProfit : {
				required : '请输入登录账号',
				number	: '请输入数字'
			},
			money :  {
				required : '请输入出入金',
				number	: '请输入数字'
			},
			profitLossTotal : {
				required : '请输入当日平仓盈亏合计',
				number : '请输入数字'
			},
			canDeposit : {
				required : '请输入可用保证金',
				number : '请输入数字'
			},
			curFeeTotal : {
				required : '请输入当日手续费合计',
				number : '请输入数字'
			},
			canDeposit : {
				required : '请输入可用保证金',
				number : '请输入数字'
			},
			occupyDeposit : {
				required : '请输入占用保证金',
				number : '请输入数字'
			},
			preDelayFee : {
				required : '请输入上日延期费',
				number : '请输入数字'
			},
			riskRate : {
				required : '请输入风险率'
			},
			freezeDeposit : {
				required : '请输入冻结保证金',
				number : '请输入数字'
			},
			freezeFee : {
				required : '请输入冻结手续费',
				number : '请输入数字'
			}
		}
	});
});