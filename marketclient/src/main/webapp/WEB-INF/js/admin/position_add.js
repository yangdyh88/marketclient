$(function() {
	$("#addPositionForm").validate({
		rules : {
			positionNo : 'required',
			goodsName : 'required',
			buildPosition : {
				required : true,
				digits : true
			},
			openPosition : {
				required : true,
				digits : true
			},
			buildPositionPrice : {
				required : true,
				number : true
			},
			openPositionPrice : {
				required : true,
				number : true
			},
			occupyDeposit: {
				required : true,
				number : true
			},
			ratio : 'required',
		},
		messages : {
			positionNo : {
				required : "请输入持仓单号"
			},
			goodsName : {
				required : "请输入商品名称"
			},
			buildPosition : {
				required : "请输入建仓量",
				digits : "请输入整数"
			},
			openPosition : {
				required : "请输入持仓量",
				digits : "请输入整数"
			},
			buildPositionPrice : {
				required : "请输入建仓价",
				number : "请输入数字"
			},
			openPositionPrice : {
				required : "请输入持仓价",
				number : "请输入数字"
			},
			ratio : {
				required : "请输入持仓保证金比例"
			},
			occupyDeposit : {
				required : "请输入占用保证金"
			}
		}
	});
});
