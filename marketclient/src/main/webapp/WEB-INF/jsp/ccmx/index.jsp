<%@ page language="java" import="java.util.*"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/_includes.jsp" %>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>银大(天津)贵金属交易系统(客户版)2.7</title>
<script type="text/javascript" src="/static/vendor/jquery/jquery.min.js"></script>
<style type="text/css">
table { font-size:12px;}
a,img{border:0;}
body{font:8px/180% Arial, Helvetica, sans-serif, "新宋体";}
.tab1{border-top:#cccccc border-bottom:#cccccc }
.menu{border-right:#f0f0f0}
.menu ul{ margin:0; padding:0}

.menu li{float:left;cursor:pointer;border-left:#cccccc color:#f0f0f0;font-size:14px;overflow:hidden;background:#f0f0f0;}
.menu li.off{background:#FFFFFF;color:#336699;}
.menudiv{border-left:#cccccc border-right:#cccccc background:#fefefe}
</style>
<script language=javascript>setInterval("date()", 1000);
function setTab(name,cursel){
	cursel_0=cursel;
	for(var i=1; i<=links_len; i++){
		var menu = document.getElementById(name+i);
		var menudiv = document.getElementById("con_"+name+"_"+i);
		if(i==cursel){
			menu.className="off";
			menudiv.style.display="block";
		}
		else{
			menu.className="";
			menudiv.style.display="none";
		}
	}
}
function Next(){                                                        
	cursel_0++;
	if (cursel_0>links_len)cursel_0=1
	setTab(name_0,cursel_0);
} 
var name_0='one';
var cursel_0=1;
//var ScrollTime=3000;//循环周期（毫秒）
var links_len,iIntervalId;
onload=function(){
	var links = document.getElementById("tab1").getElementsByTagName('li')
	links_len=links.length;
	for(var i=0; i<links_len; i++){
		links[i].onmouseover=function(){
			clearInterval(iIntervalId);
			this.onmouseout=function(){
				iIntervalId = setInterval(Next,ScrollTime);;
			}
		}
	}
	document.getElementById("con_"+name_0+"_"+links_len).parentNode.onmouseover=function(){
		clearInterval(iIntervalId);
		this.onmouseout=function(){
			iIntervalId = setInterval(Next,ScrollTime);;
		}
	}
	setTab(name_0,cursel_0);
	iIntervalId = setInterval(Next,ScrollTime);
}
</script>
</head>



   <body>
       <table width="1000" border="1" align="center">
  <tr>
    <td width="1300" height="28" colspan="2"><img src="/static/img/1.jpg" height="28" width="1750" /></td>
  </tr>
  <tr>
    <td width="190" height="900" rowspan="3" valign="top"><img src="/static/img/2.jpg" height="1000" width="190" /></td>
    <td width="1374" height="90" valign="top"><img src="/static/img/3.jpg" height="90" width="1555" /></td>
  </tr>
  <tr>
    <td height="864" valign="top" bgcolor="#f0f0f0"><div class="tab1" id="tab1">
		      <div class="menu">
		          <ul>
			         <li id="one1" onClick="setTab('one',1)"><img src="/static/img/4.jpg" /></li>
			         <li><img src="/static/img/5.jpg" /></li>
			         <li id="one2" onClick="setTab('one',2)"><img src="/static/img/6.jpg" /></li>
					 <li><img src="/static/img/7.jpg" /></li>
					 <li><img src="/static/img/11.jpg" /></li>
					 <li><img src="/static/img/12.jpg" /></li>
					 <li><img src="/static/img/13.jpg" /></li>
					 <li><img src="/static/img/14.jpg" /></li>
			         <br />
		          </ul>
	          </div>
              <div style="clear:both;">
			  <div class="menudiv">
		<div id="con_one_1"><fieldset><legend>持仓明细</legend><div style="background:#FFF;">
		<img src="/static/img/8.jpg" />
		<table  id="admin_list_tab1" border="1" cellpadding="0" cellspacing="0">
		<!-- <thead>
    <tr Bgcolor= #f0f0f0>
	    <td width="79">&nbsp持仓单号</td><td width="79">&nbsp商品名称</td><td width="58">&nbsp买/卖</td>
           <td width="67">&nbsp建仓量</td><td width="79">&nbsp持仓数量</td><td width="67">&nbsp建仓价</td><td width="67">&nbsp持仓价</td><td width="67">&nbsp平仓价</td>
           <td width="67">&nbsp止损价</td><td width="67">&nbsp止盈价</td><td width="110">&nbsp当日浮动盈亏</td><td width="120">&nbsp持仓保证金比例</td><td width="91">&nbsp占用保证金</td>
           <td width="94">&nbsp建仓时间</td><td width="100">&nbsp电话交易员</td>
           
	</tr>
		</thead> -->
		<tbody>
	<c:forEach items="${positionList }" var="position"><tr bgcolor="#ffffff">
           <td width="79">${position.positionNo}</td>
								<td width="81">${position.goodsName}</td>
								<td width="58">
								    <c:choose>
                                        <c:when test="${empty position.buyOrSell}">
                                        </c:when>
                                        <c:when test="${position.buyOrSell == 0}">
                                        	买入
                                        </c:when>
                                        <c:when test="${position.buyOrSell == 1}">
                             		               卖出
                                        </c:when>
								    </c:choose>
                                </td>
                                <td width="67">${position.buildPosition}</td>
								<td width="76">${position.openPosition}</td>
								<td width="65">${position.buildPositionPrice}</td>
								<td width="67">${position.openPositionPrice}</td>
								<td width="66">${position.closePositionPrice}</td>
								<td width="66">--</td>
								<td width="66">--</td>
								<td width="84">
									<c:choose>
                                        <c:when test="${empty position.curProfitLoss}">
                                        </c:when>
                                        <c:when test="${position.curProfitLoss >= 0}">
                                        	<font style="color:red">${position.curProfitLoss}</font>
                                        </c:when>
                                        <c:when test="${position.curProfitLoss < 0}">
                                        		<font style="color:green">${position.curProfitLoss}</font>
                                        </c:when>
								    </c:choose>
								</td>
								<td width="94">${position.ratio}</td>
								<td width="91">${position.occupyDeposit}</td>
								<td width="121"><fmt:formatDate value="${position.createTime}" pattern="yyyy-MM-dd"/></td>
								<td width="88">${position.phoneDealer}</td>
        </tr></c:forEach>
        </tbody>
</table><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<br /><br />

</div></fieldset><fieldset><legend>指价委托单</legend><div style="background:#FFF;">
<img src="/static/img/9.jpg" />
<!-- <table border="1" cellpadding="0" cellspacing="0">
<tr Bgcolor= #f0f0f0>
	    <td>&nbsp委托单号</td><td>&nbsp商品名称</td><td>&nbsp买/卖</td>
        <td>&nbsp数量</td><td>&nbsp委托价</td><td>&nbsp止损价</td><td>&nbsp止盈价</td>
        <td>&nbsp冻结保证金</td><td>&nbsp冻结手续费</td><td>&nbsp委托单状态</td><td>&nbsp委托类型</td><td>&nbsp有效期限</td>
        <td>&nbsp电话交易员</td>
	</tr>
</table> --><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</div></fieldset><fieldset><legend>账户信息</legend><div style="background:#FFF;">
<img src="/static/img/10.jpg" />
<table border="1" cellpadding="0" cellspacing="0" id="admin_list_tab2">
<!-- <thead><tr Bgcolor= #f0f0f0>
	    <td>&nbsp账户名称</td><td>&nbsp登陆账号</td><td>&nbsp期初权益</td>
        <td>&nbsp当前权益</td><td>&nbsp出入金</td><td>&nbsp浮动盈亏</td><td>&nbsp当日平仓盈亏合计</td>
        <td>&nbsp可用保证金</td><td>&nbsp当日手续费合计</td><td>&nbsp占用保证金</td><td>&nbsp上日延期费</td><td>&nbsp风险率</td>
        <td>&nbsp冻结保证金</td><td>&nbsp冻结手续费</td><td>&nbsp账号状态</td>
	</tr></thead> --><tbody>
	<c:forEach items="${managerList}" var="manager">
							<tr bgcolor="#ffffff">
								<td width="87">${manager.userName}</td>
								<td width="116">${manager.userCode}</td>
								<td width="87">${manager.primeProfit }</td>
                                <td width="86">${manager.curProfit}</td>
                                <td width="70">${manager.money}</td>
								<td width="87">
									<c:choose>
                                        <c:when test="${empty manager.profitLoss}">
                                        </c:when>
                                        <c:when test="${manager.profitLoss >= 0}">
                                        	<font style="color:red">${manager.profitLoss}</font>
                                        </c:when>
                                        <c:when test="${manager.profitLoss < 0}">
                                        		<font style="color:green">${manager.profitLoss}</font>
                                        </c:when>
								    </c:choose>
								</td>
								<td width="140">${manager.profitLossTotal}</td>
								<td width="100">${manager.occupyDeposit}</td>
								<td width="125">${manager.canDeposit}</td>
								<td width="100">${manager.curFeeTotal}</td>
								<td width="100">${manager.preDelayFee}</td>
								<td width="70">${manager.riskRate}</td>
								<td width="99">${manager.freezeDeposit}</td>
								<td width="100">${manager.freezeFee}</td>
								<td width="87">正常</td>
							</tr>
						</c:forEach>
	</tbody>
</table><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
</div></fieldset>
      </div>
	  <div id="con_one_2" style="display:none;" align="left">
	  <img  width="1095px"  height="400px" src="/attach/adver/88.jpg" />
	<!--  <img src="/static/img/8.jpg"> -->
	  <!-- <fieldset><legend>成交查询</legend>
	  <table border="1" cellpadding="0" cellspacing="0">
	  <tr Bgcolor= #f0f0f0>
	    <td>&nbsp成交单号</td><td>&nbsp商品名称</td><td>&nbsp买/卖</td>
        <td>&nbsp数量</td><td>&nbsp成交价</td><td>&nbsp盈亏</td>
        <td>&nbsp手续费</td><td>&nbsp委托单号</td><td>&nbsp持仓单号</td><td>&nbsp建仓/平仓</td>
        <td>&nbsp交易对象</td><td>&nbsp成交类型</td><td>&nbsp操作类型</td><td>&nbsp成交时间</td>
	</tr><tr bgcolor="#ffffff">
	    <td>123</td><td>456</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
	    <td></td><td></td><td></td><td></td><td></td>
	</tr>
	  </table><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
	  <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
	  <br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
	  </fieldset> -->
      </div>
		 </div></td>
  </tr>
  
  <tr>
    <td height="10" align="right" bgcolor="#f0f0f0">&nbsp;实盘环境&nbsp&nbsp&nbsp&nbsp登陆账号
   <c:forEach items="${managerList}" var="manager">${manager.userCode}</c:forEach><img src="/static/img/15.jpg" /><span id="time"></span></td>
  </tr>
</table>
 <script type="text/javascript" src="/static/js/ccmx/ccmx_list.js"></script>
</body>
<script type="text/javascript">
function date(){
var now = new Date();
    var year = now.getFullYear();       //年
    var month = now.getMonth() + 1;     //月
    var day = now.getDate();            //日
   
    var hh = now.getHours();            //时
    var mm = now.getMinutes();          //分
    var ss = now.getSeconds();          //秒
    var myDate = year+"-"+month+"-"+day+"  "+hh+":"+mm+":"+ss
      $('#time').html(myDate);
}
</script>
</html>
