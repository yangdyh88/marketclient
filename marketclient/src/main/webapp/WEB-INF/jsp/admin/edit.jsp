<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/_includes.jsp"%>
<jsp:include page="/WEB-INF/jsp/common/_head.jsp"/>
<div class="ui grid">
    <div class="row">
        <div class="column">
            <div class="ui two column grid">
                <div class="three wide column">
                    <jsp:include page="/WEB-INF/jsp/common/_sidebar.jsp">
                        <jsp:param name="index" value="4"/>
                    </jsp:include>
                </div>
                <div id="container" class="thirteen wide column">
                    <div class="alert alert-success w200 center-block text-center " style="display: none; " role="alert"></div>
                    <ol class="breadcrumb">
                        <li><a href="/">后台管理</a></li>
                        <li><a href="/admin/ls">主菜单</a></li>
                        <li class="active">修改持仓</li>
                    </ol>
			<form class="form-horizontal" id="addPositionForm" method="post" action="/admin/edit">
					<input type="hidden" name="id" value="${position.id }">
				<div class="form-group">
					<label for="positionNo" class="col-sm-3 control-label">持仓单号</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="positionNo" name="positionNo"
							autocomplete="off" placeholder="请输入持仓单号" value="${position.positionNo}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label for="goodsName" class="col-sm-3 control-label">商品名称</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="goodsName" name="goodsName"
							autocomplete="off" placeholder="请输入商品名称" value="${position.goodsName}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label  for="buyOrSell" class="col-sm-3 control-label">买/卖</label>
					<div class="col-sm-9">
                       	 <label class="radio-inline"> 
							<input type="radio"  name="buyOrSell"  value="0" ${position.buyOrSell==0 ? 'checked' : ''}>买入
						</label>
						<label class="radio-inline"> 
							<input type="radio"  name="buyOrSell" value="1" ${position.buyOrSell==1 ? 'checked': ''}>卖出
						</label>
					</div>
				</div>
				<div class="form-group">
					<label for="buildPosition" class="col-sm-3 control-label">建仓量</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="buildPosition" name="buildPosition"
							autocomplete="off" placeholder="请输入建仓量" value="${position.buildPosition}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label for="openPosition" class="col-sm-3 control-label">持仓量</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="openPosition" name="openPosition"
							autocomplete="off" placeholder="请输入持仓量" value="${position.openPosition}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label for="buildPositionPrice" class="col-sm-3 control-label">建仓价</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="buildPositionPrice" name="buildPositionPrice"
							autocomplete="off" placeholder="请输入建仓价" value="${position.buildPositionPrice}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label for="openPositionPrice" class="col-sm-3 control-label">持仓价</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="openPositionPrice" name="openPositionPrice"
							autocomplete="off" placeholder="请输入持仓价" value="${position.openPositionPrice}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label for="ratio" class="col-sm-3 control-label">持仓保证金比例</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="ratio" name="ratio"
							autocomplete="off" placeholder="请输入持仓保证金比例" value="${position.ratio}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label for="occupyDeposit" class="col-sm-3 control-label">占用保证金</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="occupyDeposit" name="occupyDeposit"
							autocomplete="off" placeholder="请输入占用保证金" value="${position.occupyDeposit}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label for="name" class="col-sm-3 control-label">电话交易员</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="phoneDealer" name="phoneDealer"
							autocomplete="off" placeholder="请输入电话交易员" value="${position.phoneDealer}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-2">
						<button type="submit" class="btn btn-block btn-orange">提交</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</div>
    </div>
<jsp:include page="/WEB-INF/jsp/common/_foot.jsp"></jsp:include>
<script src="/static/vendor/upload/js/vendor/jquery.ui.widget.js"></script>
<script src="/static/vendor/upload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/static/vendor/jquery.validate/jquery.validate.js"></script>
<script type="text/javascript" src="/static/js/admin/position_add.js"></script>