<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/_includes.jsp"%>
<jsp:include page="/WEB-INF/jsp/common/_head.jsp"/>
<div class="ui grid">
    <div class="row">
        <div class="column">
            <div class="ui two column grid">
                <div class="three wide column">
                    <jsp:include page="/WEB-INF/jsp/common/_sidebar.jsp">
                        <jsp:param name="index" value="4"/>
                    </jsp:include>
                </div>
                <div id="container" class="thirteen wide column">
                    <div class="alert alert-success w200 center-block text-center " style="display: none; " role="alert"></div>
                    <ol class="breadcrumb">
                        <li><a href="/">后台管理</a></li>
                        <li><a href="/admin/ls">主菜单</a></li>
                        <li class="active">持仓明细</li>
                    </ol>
			<form class="form-horizontal" id="viewPositionForm" method="post"action="#">
				
				<div class="form-group">
					<label class="col-sm-3 control-label">持仓单号</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${position.positionNo}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">商品名称</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${position.goodsName}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">买/卖</label>
					<div class="col-sm-5">
						 <p class="form-control-static">
						 	 <c:choose>
								<c:when test="${position.buyOrSell==0}">买入</c:when>
								<c:when test="${position.buyOrSell==1}">卖出</c:when>
								<c:otherwise></c:otherwise>
						     </c:choose>
						 </p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">建仓量</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${position.buildPosition}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">持仓量</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${position.openPosition}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">建仓价</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${position.buildPositionPrice}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">持仓价</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${position.openPositionPrice}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">持仓保证金比例</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${position.ratio}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">持仓价</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${position.openPositionPrice}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">占用保证金</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${position.occupyDeposit}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">创建时间</label>
					<div class="col-sm-5">
						 <p class="form-control-static">
						 	<fmt:formatDate value="${position.createTime}" type="both"/>
						 </p>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</div>
    </div>
<jsp:include page="/WEB-INF/jsp/common/_foot.jsp"></jsp:include>
