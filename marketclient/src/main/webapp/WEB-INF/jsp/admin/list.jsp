<%@ page language="java" import="java.util.*"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/_includes.jsp" %>
<jsp:include page="/WEB-INF/jsp/common/_head.jsp"/>
<script type="text/javascript" src="/static/vendor/jquery/jquery.min.js"></script>
<div class="ui grid">
	<div class="row">
		<div class="column">
			<div class="ui two column grid">
				<div class="three wide column">
					<jsp:include page="/WEB-INF/jsp/common/_sidebar.jsp">
						<jsp:param name="index" value="1"/>
					</jsp:include>
				</div>
				<div id="container" class="thirteen wide column">
					<ol class="breadcrumb">
						<li><a href="/">后台管理</a></li>
						<li><a href="/admin/list">主菜单</a></li>
						<li class="active">交易列表</li>
					</ol>
					<fieldset name="持仓明细">
					<table id="admin_list_tab1" class="table table-hover">
						<thead>
						<tr>
							<th>持仓单号</th>
							<th>商品名称</th>
							<th>买/卖</th>
							<th>建仓量</th>
							<th>持仓量</th>
							<th>建仓价</th>
							<th>持仓价</th>
							<th>平仓价</th>
							<th>止损价</th>
							<th>止盈价</th>
							<th>当日浮动盈亏</th>
							<th>持仓保证金比例</th>
							<th>占用保证金</th>
							<th>创建时间</th>
							<th>电话交易员</th>
                            <th>操作</th>
						</tr>
						</thead>
						<tbody>
						<c:forEach items="${positionList}" var="position">
							<tr>
								<td>${position.positionNo}</td>
								<td>${position.goodsName}</td>
								<td>
								    <c:choose>
                                        <c:when test="${empty position.buyOrSell}">
                                        </c:when>
                                        <c:when test="${position.buyOrSell == 0}">
                                        	买入
                                        </c:when>
                                        <c:when test="${position.buyOrSell == 1}">
                             		               卖出
                                        </c:when>
								    </c:choose>
                                </td>
                                <td>${position.buildPosition}</td>
								<td>${position.openPosition}</td>
								<td>${position.buildPositionPrice}</td>
								<td>${position.openPositionPrice}</td>
								<td>${position.closePositionPrice}</td>
								<td>--</td>
								<td>--</td>
								<td>
									<c:choose>
                                        <c:when test="${empty position.curProfitLoss}">
                                        </c:when>
                                        <c:when test="${position.curProfitLoss >= 0}">
                                        	<font style="color:red">${position.curProfitLoss}</font>
                                        </c:when>
                                        <c:when test="${position.curProfitLoss < 0}">
                                        		<font style="color:green">${position.curProfitLoss}</font>
                                        </c:when>
								    </c:choose>
								</td>
								<td>${position.ratio}</td>
								<td>${position.occupyDeposit}</td>
								<td><fmt:formatDate value="${position.createTime}" pattern="yyyy-MM-dd"/></td>
								<td>${position.phoneDealer}</td>
								<td> <a class="btn btn-info js_broadcast" href="/admin/${position.id }/edit">修改</a></td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
					</fieldset>
					<table id="admin_list_tab2" class="table table-hover">
						<thead>
						<tr>
							<th>账号名称</th>
							<th>登录账号</th>
							<th>期初权益</th>
							<th>当前权益</th>
							<th>出入金</th>
							<th>浮动盈亏</th>
							<th>当日平仓盈亏合计</th>
							<th>占用保证金</th>
							<th>上期延期费</th>
							<th>风险率</th>
							<th>冻结保证金</th>
							<th>冻结手续费</th>
							<th>账号状态</th>
                            <th>操作</th>
						</tr>
						</thead>
						<tbody>
						<c:forEach items="${managerList}" var="manager">
							<tr>
								<td>${manager.userName}</td>
								<td>${manager.userCode}</td>
								<td>${manager.primeProfit }</td>
                                <td>${manager.curProfit}</td>
								<td>${manager.money}</td>
								<td>
									<c:choose>
                                        <c:when test="${empty manager.profitLoss}">
                                        </c:when>
                                        <c:when test="${manager.profitLoss >= 0}">
                                        	<font style="color:red">${manager.profitLoss}</font>
                                        </c:when>
                                        <c:when test="${manager.profitLoss < 0}">
                                        		<font style="color:green">${manager.profitLoss}</font>
                                        </c:when>
								    </c:choose>
								</td>
								<td>${manager.profitLossTotal}</td>
								<td>${manager.occupyDeposit}</td>
								<td>${manager.preDelayFee}</td>
								<td>${manager.riskRate}</td>
								<td>${manager.freezeDeposit}</td>
								<td>${manager.freezeFee}</td>
								<td>正常</td>
								<td> <a class="btn btn-info js_broadcast" href="/manager/${manager.id}/edit">修改</a></td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<jsp:include page="/WEB-INF/jsp/common/_foot.jsp"></jsp:include>
 <script type="text/javascript" src="/static/js/admin/admin_list.js"></script>




