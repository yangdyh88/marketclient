<%--
  Created by IntelliJ IDEA.
  User: Jerry
  Date: 2014/10/5
  Time: 2:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/jsp/common/_includes.jsp" %>
<ul class="pagination pull-right ml10">
    <c:set var="defaultPage" value="${param.pageNo > 0 ? param.pageNo : 0}"/>
    <c:set var="count" value="5"/>
    <li>
        <c:if test="${param.pageNo > 1}">
    <li><a href="javaScript:;" id="previous" data-type="${param.pageNo-1}">&laquo;</a></li>
    </c:if>
    <c:set var="start" value="${defaultPage - count > 0 ? defaultPage - count : 1}"/>
    <c:set var="end" value="${defaultPage + count < paginate.totalPage ? defaultPage+ count : paginate.totalPage}"/>
    <c:forEach begin="${start }" end="${end }" var="pageNo">
        <li ${pageNo == param.pageNo ? 'class="active"' :''}><a href="javaScript:;" data-type="${pageNo}">${pageNo}</a>
        </li>
    </c:forEach>
    <c:if test="${paginate.pageNo < paginate.totalPage }">
        <li>
        <li><a href="javaScript:;" id="next" data-type="${param.pageNo+1}">&raquo;</a></li>
    </c:if>
</ul>
<c:if test="${paginate.totalCount > 0 }">
    <div class="mr5 pull-left">共有${paginate.totalCount }条信息</div>
</c:if>
<script type="text/javascript" src="/static/js/page.js"></script>