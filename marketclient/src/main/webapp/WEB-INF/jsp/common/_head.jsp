<%@ include file="/WEB-INF/jsp/common/_includes.jsp" %>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>银大(天津)贵金属交易系统(客户版)2.7</title>
<link rel="stylesheet" type="text/css" href="/static/css/czd.css">
<script type="text/javascript" src="/static/vendor/jquery/jquery.min.js"></script>
</head>
<body>
	<div class="czd-header clearfix">
		<div class="cleft">
			<a href="#index"><span class="sub-title">银大(天津)贵金属交易系统(客户版)2.7</span></a>
		</div>
		<div class="cright">
			下午好<c:if test="${not empty SESSION_USER }">，${SESSION_USER.userName }<a class="ml20 logout" href="/logout">退出</a></c:if>
		</div>
	</div>