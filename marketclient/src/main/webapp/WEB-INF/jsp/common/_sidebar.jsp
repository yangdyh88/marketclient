<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/_includes.jsp" %>
<div class="ui teal vertical fluid gusi menu">
    <div class="item">
        <i class="sitemap icon"></i> <b>主菜单</b>
        <div class="menu">
            <a class="item  ${param.index == 1 ? 'active' :''}" href="/admin/ls"><i class="home icon"></i>交易列表</a>
            <a class="item  ${param.index == 2 ? 'active' :''}" href="/admin/add"><i class="home icon"></i>新建持仓</a>
        </div>
    </div>
</div>
