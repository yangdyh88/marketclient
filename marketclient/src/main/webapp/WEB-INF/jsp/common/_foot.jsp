<script type="text/javascript" src="/static/js/global.js"></script>
<script type="text/javascript" src="/static/vendor/jquery/jquery.js"></script>
<script type="text/javascript" src="/static/vendor/bootstrap/js/bootstrap.min.js"></script>
<script data-main="/static/js/app" src="/static/vendor/qtip2/jquery.qtip.min.js"></script>
<script type="text/javascript" src="/static/vendor/underscore-min.js"></script>
<script type="text/javascript" src="/static/vendor/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="/static/js/util.js"></script>
<script type="text/javascript" src="/static/vendor/socket.io-1.0.6.js"></script>
<script type="text/javascript" src="/static/js/message.js"></script>
<script type="text/javascript">
	var userName = '${SESSION_USER.userName}';
</script>
</body>
</html>