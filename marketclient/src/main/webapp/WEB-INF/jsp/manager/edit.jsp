<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/_includes.jsp"%>
<jsp:include page="/WEB-INF/jsp/common/_head.jsp"/>
<div class="ui grid">
    <div class="row">
        <div class="column">
            <div class="ui two column grid">
                <div class="three wide column">
                    <jsp:include page="/WEB-INF/jsp/common/_sidebar.jsp">
                        <jsp:param name="index" value="4"/>
                    </jsp:include>
                </div>
                <div id="container" class="thirteen wide column">
                    <div class="alert alert-success w200 center-block text-center " style="display: none; " role="alert"></div>
                    <ol class="breadcrumb">
                        <li><a href="/">后台管理</a></li>
                        <li><a href="/admin/ls">主菜单</a></li>
                        <li class="active">修改账户</li>
                    </ol>
			<form class="form-horizontal" id="addManagerForm" method="post" action="/manager/edit">
					<input type="hidden" name="id" value="${manager.id }">
				<div class="form-group">
					<label for="userName" class="col-sm-3 control-label">账户名称</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="userName" name="userName"
							autocomplete="off" placeholder="请输入账户名称" value="${manager.userName}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label for="userCode" class="col-sm-3 control-label">登录账号</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="userCode" name="userCode"
							autocomplete="off" placeholder="请输入登录账号" value="${manager.userCode}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label for="primeProfit" class="col-sm-3 control-label">期初权益</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="primeProfit" name="primeProfit"
							autocomplete="off" placeholder="请输入期初权益" value="${manager.primeProfit}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label for="money" class="col-sm-3 control-label">出入金</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="money" name="money"
							autocomplete="off" placeholder="请输入出入金" value="${manager.money}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label for="profitLossTotal" class="col-sm-3 control-label">当日平仓盈亏合计</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="profitLossTotal" name="profitLossTotal"
							autocomplete="off" placeholder="请输入当日平仓盈亏合计" value="${manager.profitLossTotal}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label for="canDeposit" class="col-sm-3 control-label">可用保证金</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="canDeposit" name="canDeposit"
							autocomplete="off" placeholder="请输入可用保证金" value="${manager.canDeposit}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label for="curFeeTotal" class="col-sm-3 control-label">当日手续费合计</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="curFeeTotal" name="curFeeTotal"
							autocomplete="off" placeholder="请输入当日手续费合计" value="${manager.curFeeTotal}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label for="occupyDeposit" class="col-sm-3 control-label">占用保证金</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="occupyDeposit" name="occupyDeposit"
							autocomplete="off" placeholder="请输入占用保证金" value="${manager.occupyDeposit}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label for="preDelayFee" class="col-sm-3 control-label">上日延期费</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="preDelayFee" name="preDelayFee"
							autocomplete="off" placeholder="请输入上日延期费" value="${manager.preDelayFee}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label for="riskRate" class="col-sm-3 control-label">风险率</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="preDelayFee" name="preDelayFee"
							autocomplete="off" placeholder="请输入上日延期费" value="${manager.riskRate}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label for="freezeDeposit" class="col-sm-3 control-label">冻结保证金</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="freezeDeposit" name="freezeDeposit"
							autocomplete="off" placeholder="请输入冻结保证金" value="${manager.freezeDeposit}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<label for="freezeFee" class="col-sm-3 control-label">冻结手续费</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="freezeFee" name="freezeFee"
							autocomplete="off" placeholder="请输入冻结保证金" value="${manager.freezeFee}">
						<i class="icon-ok-sign out"></i>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-2">
						<button type="submit" class="btn btn-block btn-orange">提交</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</div>
</div>
<jsp:include page="/WEB-INF/jsp/common/_foot.jsp"></jsp:include>
<script src="/static/vendor/upload/js/vendor/jquery.ui.widget.js"></script>
<script src="/static/vendor/upload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/static/vendor/jquery.validate/jquery.validate.js"></script>
<script type="text/javascript" src="/static/js/admin/manager_add.js"></script>