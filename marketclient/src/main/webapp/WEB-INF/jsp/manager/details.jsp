<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/_includes.jsp"%>
<jsp:include page="/WEB-INF/jsp/common/_head.jsp"/>
<div class="ui grid">
    <div class="row">
        <div class="column">
            <div class="ui two column grid">
                <div class="three wide column">
                    <jsp:include page="/WEB-INF/jsp/common/_sidebar.jsp">
                        <jsp:param name="index" value="4"/>
                    </jsp:include>
                </div>
                <div id="container" class="thirteen wide column">
                    <div class="alert alert-success w200 center-block text-center " style="display: none; " role="alert"></div>
                    <ol class="breadcrumb">
                        <li><a href="/">后台管理</a></li>
                        <li><a href="/admin/ls">主菜单</a></li>
                        <li class="active">持仓明细</li>
                    </ol>
			<form class="form-horizontal" id="viewPositionForm" method="post"action="#">
				<div class="form-group">
					<label class="col-sm-3 control-label">账户名称</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${manager.userName}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">登录账号</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${manager.userCode}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">期初权益</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${manager.primeProfit}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">出入金</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${manager.money}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">当日平仓盈亏合计</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${manager.profitLossTotal}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">可用保证金</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${manager.canDeposit}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">当日手续费合计</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${manager.curFeeTotal}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">占用保证金</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${manager.occupyDeposit}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">上日延期费</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${manager.preDelayFee}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">风险率</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${manager.riskRate}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">冻结保证金</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${manager.freezeDeposit}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">冻结手续费</label>
					<div class="col-sm-5">
						 <p class="form-control-static">${manager.freezeFee}</p>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
</div>
    </div>
<jsp:include page="/WEB-INF/jsp/common/_foot.jsp"></jsp:include>
