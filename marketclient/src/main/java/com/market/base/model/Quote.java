package com.market.base.model;

import java.io.Serializable;
import java.util.List;

import com.market.model.Manager;
import com.market.model.Position;

/*
 * 报价信息实体对象
 */
public class Quote  implements Serializable {
	private static final long serialVersionUID = 3496720833800154481L;
	
	private List<Position> positionList;
	private List<Manager> manageList;
	public List<Position> getPositionList() {
		return positionList;
	}
	public void setPositionList(List<Position> positionList) {
		this.positionList = positionList;
	}
	public List<Manager> getManageList() {
		return manageList;
	}
	public void setManageList(List<Manager> manageList) {
		this.manageList = manageList;
	}
}
