package com.market.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.market.common.Constants;

/**
 * 得到最新报价
 * @author yangjun
 *
 */
public class QuoteUtils {
	public static float getNewQuote2() {
		float price = 0;
	
		InputStream inStream = null;
		try {
			URL url = new URL(Constants.QUOTE_URL_PATH);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			inStream = con.getInputStream();
			BufferedReader bufReader = new BufferedReader(new InputStreamReader(inStream,"UTF-8"));
			String line = "";
			StringBuffer sb = new StringBuffer();
			while((line = bufReader.readLine()) != null) {
				sb.append(line);
			}
			String s = sb.toString();
			 //代码,名称,最新价格,涨跌,涨跌幅,买价,卖价,成交量,成交金额,开盘价,最高价,最低价,昨收盘价,时间\n
			 if(StringUtils.isNotBlank(s)) {
				 String[] arrays = s.split(",");
				 if(arrays!=null && arrays.length > 2) {
					 //平仓价=最新价格
					 price = Float.parseFloat(arrays[2]); 
				 }
			 }
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		  return price;
	}
	
	public static float getNewQuote() {
		float price = (float) 0;
		Map<String, String> params = new HashMap<String, String>();
		params.put("symbol", "TJXAG");
		String s = HttpClientUtils.doPost(Constants.QUOTE_URL_PATH, params);
		String[] arrays = s.split(",");
		if(arrays!=null && arrays.length > 2) {
			price = Float.parseFloat(arrays[2]);
		}
		
		return price;
	}
	public static void main(String[] args) {
		System.out.println(QuoteUtils.getNewQuote());
	}
}
