package com.market.utils;

import java.util.List;

public class ListUtils {

	public static boolean isEmpty(List<?> list) {
		boolean result = false;
		if (list == null || list.size() == 0) {
			result = true;
		}
		return result;
	}

	public static boolean isNotEmpty(List<?> list) {
		boolean result = false;
		
		if (list != null && list.size() > 0) {
			result = true;
		}
		return result;
	}
}
