package com.market.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.market.model.Manager;
import com.market.service.ManagerService;

@Controller
@RequestMapping("/manager")
public class ManagerController extends BaseController {
	@Autowired
	private ManagerService managerService;
	
	@RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
	public String edit(@PathVariable int id, Model model) {
		Manager manager = managerService.findManagerById(id);
		model.addAttribute("manager", manager);
		
		if(manager!=null) {
			model.addAttribute("manager", manager);
			return "/manager/edit";
		}
		return error(model, "账户不存在", "账户信息不存在");
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public String edit(Manager manager, Model model) {
		if(manager!=null) {
			boolean fln = managerService.update(manager);
			if(!fln) return error(model, "操作失败", "更新账户信息失败");
		
			return "redirect:/manager/" + manager.getId() + "/details";
		}
		return error(model, "操作失败", "更新账户信息失败");
	}
	
	@RequestMapping(value = "/{id}/details", method = RequestMethod.GET)
	public String details(@PathVariable int id, Model model) {
		Manager manager = managerService.findManagerById(id);
		if(manager!=null) {
			model.addAttribute("manager", manager);
		}
		
		return "/manager/details";
	}
	
}
