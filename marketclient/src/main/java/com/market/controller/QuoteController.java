package com.market.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.market.common.Constants;
import com.market.utils.HttpClientUtils;

@Controller
@RequestMapping("/quote")
public class QuoteController extends BaseController {

	@RequestMapping(value={""}, method = RequestMethod.POST)
	public String getQuote(HttpServletResponse response) throws IOException {
		Map<String, String> params = new HashMap<String, String>();
		params.put("symbol", "TJXAG");
		String s = HttpClientUtils.doPost(Constants.QUOTE_URL_PATH, params);
		String[] arrays = s.split(",");
		String price = "";
		if(arrays!=null && arrays.length > 2) {
			 price = arrays[2];
		}
		PrintWriter out=response.getWriter();
		out.write(price);
		out.flush();
		out.close();
		return null;
	}
}
