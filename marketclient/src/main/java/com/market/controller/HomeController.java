package com.market.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.market.model.Manager;
import com.market.model.Position;
import com.market.service.ManagerService;
import com.market.service.PositionService;
import com.market.utils.ListUtils;
import com.market.utils.QuoteUtils;

@Controller
@RequestMapping("/")

public class HomeController extends BaseController {
	
	@Autowired
	private PositionService positionService;
	@Autowired
	private ManagerService managerService; 
	
	@RequestMapping(value = {"", "/index"})
	public String listll(HttpServletRequest request, Position position, Model model) {
		List<Position> positionList =  positionService.findPositionList();
		List<Manager> managerList = managerService.findManagerList();
		List<Position> pList = new ArrayList<Position>();
		List<Manager> mList = new ArrayList<Manager>();
		float profitLoss = 0;
		float closePositionPrice = QuoteUtils.getNewQuote(); //最新价格（平仓人价）
		if(ListUtils.isNotEmpty(positionList)) {
			for(Position p : positionList) {
				Integer buyOrSell = p.getBuyOrSell();
				Float openPositionPrice = p.getOpenPositionPrice();	//持仓价
				float  curProfitLoss = 0; //当日浮动盈亏
				if(buyOrSell == 0) {//买入
					//买入：当日浮动盈亏=(平仓价-持仓价)*15
					curProfitLoss = (closePositionPrice - openPositionPrice) * 15;
				}
				else if(buyOrSell == 1) {//卖出
					//卖出：当日浮动盈亏=(持仓价-平仓价)*15
					curProfitLoss = (openPositionPrice - closePositionPrice) * 15;
				}
				profitLoss += curProfitLoss;
				p.setClosePositionPrice(closePositionPrice);
				p.setCurProfitLoss(curProfitLoss);
				
				pList.add(p);
			}
		}
		
		if(ListUtils.isNotEmpty(managerList)) {
			for(Manager m : managerList) {
				m.setProfitLoss(profitLoss);
				//当前权益 = 期初权益+浮动盈亏
				m.setCurProfit(m.getPrimeProfit() + profitLoss);
				mList.add(m);
			}
		}
		model.addAttribute("positionList", pList);
		model.addAttribute("managerList", mList);
		
		return "/ccmx/index";
	}
}