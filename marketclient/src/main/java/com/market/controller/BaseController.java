package com.market.controller;


import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.market.common.Configuration;

/**
 * Created with IntelliJ IDEA.
 * Author: GuSi
 * Create: GuSi (14-7-7 9:32)
 * Description:
 * To change this template use File | Settings | File Templates.
 */
public class BaseController {
	/*protected Manager getSessionUser(HttpServletRequest request) {
		return getSessionUser(request.getSession());
	}
	
	private Manager getSessionUser(HttpSession session) {
		if(session == null)
			return null;
		else
			return (Manager) session.getAttribute(Constants.SESSION_USER);
	}*/
	/**
	 * 系统配置参数
	 * @return
	 */
	@ModelAttribute("config")
	protected Configuration configInstance(){
		return Configuration.getInstance();
	}

	protected ModelAndView modelAndView(String url){
		return new ModelAndView(new RedirectView("url"));
	}
	/**
	 * 错误页面跳转
	 * @return
	 */
	public String error(Model model,String title,String msg){
		model.addAttribute("title",title);
		model.addAttribute("msg",msg);
		return "error";
	}
}
