package com.market.model;

import org.apache.ibatis.type.Alias;

import com.alibaba.fastjson.annotation.JSONField;
import com.market.base.model.Page;

/**
 * 员工信息
 * @author java
 *
 */
@Alias("manager")
public class Manager  extends Page {
	/**
	 * 
	 */
	private static final long serialVersionUID = 205981644873548697L;
	/*
	 * 主键
	 */
	private Integer id;
	/*
	 * 登录账号
	 */
	private String userCode;
	/*
	 * 账号名称
	 */
	private String userName;
	/*
	 * 期初权益
	 */
	private Float primeProfit;
	/*
	 * 当前权益
	 */
	private Float curProfit;
	/*
	 * 出入金
	 */
	private Float money;
	/*
	 * 浮动盈亏
	 */
	private Float profitLoss;
	/*
	 * 当日平仓盈亏合计
	 */
	private Float profitLossTotal;
	/*
	 * 可用保证金
	 */
	private Float canDeposit;
	/*
	 * 当日手续费合计
	 */
	private Float curFeeTotal;
	/*
	 * 占用保证金
	 */
	private Float occupyDeposit;
	/*
	 * 上日延期费
	 */
	private Float preDelayFee;
	/*
	 * 风险率
	 */
	private String riskRate;
	/*
	 * 冻结保证金
	 */
	private Float freezeDeposit;
	
	/*
	 * 冻结手续费
	 */
	private Float freezeFee;
	
	/*
	 * 账号状态
	 */
	private boolean accountStatus;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Float getPrimeProfit() {
		return primeProfit;
	}
	public void setPrimeProfit(Float primeProfit) {
		this.primeProfit = primeProfit;
	}
	public Float getCurProfit() {
		return curProfit;
	}
	public void setCurProfit(Float curProfit) {
		this.curProfit = curProfit;
	}
	public Float getMoney() {
		return money;
	}
	public void setMoney(Float money) {
		this.money = money;
	}
	public Float getProfitLoss() {
		return profitLoss;
	}
	public void setProfitLoss(Float profitLoss) {
		this.profitLoss = profitLoss;
	}
	public Float getProfitLossTotal() {
		return profitLossTotal;
	}
	public void setProfitLossTotal(Float profitLossTotal) {
		this.profitLossTotal = profitLossTotal;
	}
	public Float getCanDeposit() {
		return canDeposit;
	}
	public void setCanDeposit(Float canDeposit) {
		this.canDeposit = canDeposit;
	}
	public Float getCurFeeTotal() {
		return curFeeTotal;
	}
	public void setCurFeeTotal(Float curFeeTotal) {
		this.curFeeTotal = curFeeTotal;
	}
	public Float getOccupyDeposit() {
		return occupyDeposit;
	}
	public void setOccupyDeposit(Float occupyDeposit) {
		this.occupyDeposit = occupyDeposit;
	}
	public Float getPreDelayFee() {
		return preDelayFee;
	}
	public void setPreDelayFee(Float preDelayFee) {
		this.preDelayFee = preDelayFee;
	}
	public String getRiskRate() {
		return riskRate;
	}
	public void setRiskRate(String riskRate) {
		this.riskRate = riskRate;
	}
	public Float getFreezeDeposit() {
		return freezeDeposit;
	}
	public void setFreezeDeposit(Float freezeDeposit) {
		this.freezeDeposit = freezeDeposit;
	}
	public Float getFreezeFee() {
		return freezeFee;
	}
	public void setFreezeFee(Float freezeFee) {
		this.freezeFee = freezeFee;
	}
	public boolean isAccountStatus() {
		return accountStatus;
	}
	public void setAccountStatus(boolean accountStatus) {
		this.accountStatus = accountStatus;
	}
	@Override
	public String toString() {
		return "Manager [id=" + id + ", userCode=" + userCode + ", userName="
				+ userName + ", primeProfit=" + primeProfit + ", curProfit="
				+ curProfit + ", money=" + money + ", profitLoss=" + profitLoss
				+ ", profitLossTotal=" + profitLossTotal + ", canDeposit="
				+ canDeposit + ", curFeeTotal=" + curFeeTotal
				+ ", occupyDeposit=" + occupyDeposit + ", preDelayFee="
				+ preDelayFee + ", riskRate=" + riskRate + ", freezeDeposit="
				+ freezeDeposit + ", freezeFee=" + freezeFee
				+ ", accountStatus=" + accountStatus + "]";
	}
}
