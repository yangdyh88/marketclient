package com.market.model;

import org.apache.ibatis.type.Alias;

import com.market.base.model.Page;

@Alias("user")
public class User extends Page {
	/**
	 * 
	 */
	private static final long serialVersionUID = 320642195921121628L;
	/*
	 * id
	 */
	private int id;
	/*
	 * userName
	 */
	private String userName;
	/**
	 * 获得 id
	 * @return
	 */
	public int getId() {
		return id;
	}
	/**
	 * 设置 id
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * 获得 userName
	 * @return
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * 设置 userName
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + "]";
	}
}
