package com.market.model;

import java.util.Date;

import org.apache.ibatis.type.Alias;

import com.market.base.model.Page;

/**
 * 持仓实体对象
 * @author java
 *
 */
@Alias("position")
public class Position extends Page {
	/**
	 * 
	 */
	private static final long serialVersionUID = 524849765449255363L;
	/*
	 * 主键
	 */
	private Integer id;
	/*
	 * 持仓单号
	 */
	private String positionNo;  
	/*
	 * 商品名称
	 */
	private String goodsName;
	/*
	 * (0:表示买入；1：表示卖出)
	 */
	private Integer buyOrSell;
	/*
	 * 建仓量
	 */
	private Integer buildPosition;
	/*
	 * 持仓量
	 */
	private Integer openPosition;
	/*
	 * 建仓价
	 */
	private Float buildPositionPrice;
	/*
	 * 持仓价
	 */
	private Float openPositionPrice;
	/*
	 * 平仓价
	 */
	private Float closePositionPrice;
	/*
	 * 止损价
	 */
	private Float stopLossPrice;
	/*
	 * 止盈价
	 */
	private Float stopProfitPrice;
	/*
	 * 当日浮动盈亏
	 */
	private Float curProfitLoss;
	/*
	 * 持仓保证金比例
	 */
	private Float ratio;
	/*
	 * 占用保证金
	 */
	private Float occupyDeposit;
	/*
	 * 创建时间
	 */
	private Date createTime;
	/*
	 * 电话交易员
	 */
	private String phoneDealer;
	/*
	 * 账户Id
	 */
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getPositionNo() {
		return positionNo;
	}
	public void setPositionNo(String positionNo) {
		this.positionNo = positionNo;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public Integer getBuyOrSell() {
		return buyOrSell;
	}
	public void setBuyOrSell(Integer buyOrSell) {
		this.buyOrSell = buyOrSell;
	}
	public Integer getBuildPosition() {
		return buildPosition;
	}
	public void setBuildPosition(Integer buildPosition) {
		this.buildPosition = buildPosition;
	}
	public Integer getOpenPosition() {
		return openPosition;
	}
	public void setOpenPosition(Integer openPosition) {
		this.openPosition = openPosition;
	}
	public Float getBuildPositionPrice() {
		return buildPositionPrice;
	}
	public void setBuildPositionPrice(Float buildPositionPrice) {
		this.buildPositionPrice = buildPositionPrice;
	}
	public Float getOpenPositionPrice() {
		return openPositionPrice;
	}
	public void setOpenPositionPrice(Float openPositionPrice) {
		this.openPositionPrice = openPositionPrice;
	}
	public Float getClosePositionPrice() {
		return closePositionPrice;
	}
	public void setClosePositionPrice(Float closePositionPrice) {
		this.closePositionPrice = closePositionPrice;
	}
	public Float getStopLossPrice() {
		return stopLossPrice;
	}
	public void setStopLossPrice(Float stopLossPrice) {
		this.stopLossPrice = stopLossPrice;
	}
	public Float getStopProfitPrice() {
		return stopProfitPrice;
	}
	public void setStopProfitPrice(Float stopProfitPrice) {
		this.stopProfitPrice = stopProfitPrice;
	}
	public Float getCurProfitLoss() {
		return curProfitLoss;
	}
	public void setCurProfitLoss(Float curProfitLoss) {
		this.curProfitLoss = curProfitLoss;
	}
	public Float getRatio() {
		return ratio;
	}
	public void setRatio(Float ratio) {
		this.ratio = ratio;
	}
	public Float getOccupyDeposit() {
		return occupyDeposit;
	}
	public void setOccupyDeposit(Float occupyDeposit) {
		this.occupyDeposit = occupyDeposit;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getPhoneDealer() {
		return phoneDealer;
	}
	public void setPhoneDealer(String phoneDealer) {
		this.phoneDealer = phoneDealer;
	}
	@Override
	public String toString() {
		return "Position [id=" + id + ", positionNo=" + positionNo
				+ ", goodsName=" + goodsName + ", buyOrSell=" + buyOrSell
				+ ", buildPosition=" + buildPosition + ", openPosition="
				+ openPosition + ", buildPositionPrice=" + buildPositionPrice
				+ ", openPositionPrice=" + openPositionPrice
				+ ", closePositionPrice=" + closePositionPrice
				+ ", stopLossPrice=" + stopLossPrice + ", stopProfitPrice="
				+ stopProfitPrice + ", curProfitLoss=" + curProfitLoss
				+ ", ratio=" + ratio + ", occupyDeposit=" + occupyDeposit
				+ ", createTime=" + createTime + ", phoneDealer=" + phoneDealer
				+ "]";
	}
}
