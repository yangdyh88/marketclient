package com.market.service;


import org.springframework.stereotype.Service;

import com.market.base.service.BaseService;
import com.market.model.User;


@Service
public class UserService extends BaseService<User> {

	public boolean insertUser(User user) {
		if(user == null) return false;
		else return this.insert(sqlId("insert"), user);
	}
	
}
