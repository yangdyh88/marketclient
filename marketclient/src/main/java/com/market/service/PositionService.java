package com.market.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.market.base.model.Paginate;
import com.market.base.service.BaseService;
import com.market.model.Position;

@Service
public class PositionService extends BaseService<Position> {
	public boolean insert(Position position) {
		if(position == null) return false;
		else return this.insert(sqlId("insert"), position);
	}
	
	public boolean update(Position position) {
		if(position == null) return false;
		else return this.update(sqlId("update"), position);
	}
	
	public Position findPositionById(Integer id) {
		if(id == null) return null;
		else return this.selectOneById(sqlId("findPositionById"), id);
	}
	
	public List<Position> findPositionList() {
		return this.selectList(sqlId("findPositionList"));
	}
	
	public Paginate queryForPage(Position position) {
		return this.queryForPaginate(sqlId("queryForPage"), position);
	}
}
