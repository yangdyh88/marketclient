package com.market.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.market.base.service.BaseService;
import com.market.model.Manager;

@Service
public class ManagerService extends BaseService<Manager> {
	public boolean insert(Manager manager) {
		if(manager==null) return false;
		return this.insert(sqlId("insert"), manager);
	}
	
	public boolean update(Manager manager) {
		if(manager==null) return false;
		return this.update(sqlId("update"), manager);
	}
	
	public Manager findManagerById(Integer id) {
		return this.selectOne(sqlId("findManagerById"), id);
	}
	
	public List<Manager> findManagerList() {
		return this.selectList(sqlId("findManagerList"));
	}
}
