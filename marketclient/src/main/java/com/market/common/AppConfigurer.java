package com.market.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import java.io.IOException;
import java.util.Properties;

/**
 * @Package AppConfigurer
 * @Description: 启动类
 * @author GuSi
 * @date 2014-07-07
 * @version V1.0
 */
public class AppConfigurer extends PropertyPlaceholderConfigurer {
    public static final Log log = LogFactory.getLog(AppConfigurer.class);
	@Override
    protected Properties mergeProperties() throws IOException {
        Properties superProps = super.mergeProperties();
        String env = Configuration.getInstance().getEnv();
        superProps.put("env", env);
        log.info("运行环境====>>"+env);
        return superProps;
    }
}

