package com.market.common;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;



/**
 * @ClassName Configuration
 * @Description: 环境相关配置类
 * @author GuSi
 * @date  2014-07-07
 */
public class Configuration {


	private static final long serialVersionUID = -2588893938186247117L;
	private static Configuration config;
    /**
     * 当前环境，测试: development 生产： production
     */
    private String env;
    /**
     * 版本号
     */
    private String version;
    private int default_page_size;
    private String sync_room_user_count_url;
    private String attach_url;
    private String hostName;
    private String from;
    private String fromPwd;
    private String promotion_url;
    private String sync_room_isplay_url;

    public String getSync_room_isplay_url() {
        return sync_room_isplay_url;
    }

    public void setSync_room_isplay_url(String sync_room_isplay_url) {
        this.sync_room_isplay_url = sync_room_isplay_url;
    }

    public String getSync_room_user_count_url() {
        return sync_room_user_count_url;
    }

    public void setSync_room_user_count_url(String sync_room_user_count_url) {
        this.sync_room_user_count_url = sync_room_user_count_url;
    }

    /**
     * 获得 当前环境，测试: development 生产： production
     *
     * @return
     */
    public String getEnv() {
        return env;
    }

    /**
     * 设置 当前环境，测试: development 生产： production
     *
     * @param env
     */
    public void setEnv(String env) {
        this.env = env;
    }

    /**
     * 获取 版本号
     *
     * @return 版本号
     */
    public String getVersion() {
        return version;
    }

    /**
     * 设置 版本号
     *
     * @param version 版本号
     */
    public void setVersion(String version) {
        this.version = version;
    }

    public int getDefault_page_size() {
        return default_page_size;
    }

    public void setDefault_page_size(int default_page_size) {
        this.default_page_size = default_page_size;
    }

    public String getAttach_url() {
        return attach_url;
    }

    public void setAttach_url(String attach_url) {
        this.attach_url = attach_url;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getFromPwd() {
        return fromPwd;
    }

    public void setFromPwd(String fromPwd) {
        this.fromPwd = fromPwd;
    }

    public String getPromotion_url() {
        return promotion_url;
    }

    public void setPromotion_url(String promotion_url) {
        this.promotion_url = promotion_url;
    }

    private Configuration() {
	}


	public synchronized static Configuration getInstance(){
		if(config==null){
			config = new Configuration();
			config.init();
		}
		return config;
	}


	private void init(){
		PropertiesConfiguration prop = new PropertiesConfiguration();
		prop.setEncoding("utf-8");
		try {
			prop.load("global.properties");
			this.setEnv(prop.getString("env"));
			this.setVersion(prop.getString("version"));
			prop.load(this.getEnv() + "_market.properties");
            this.setDefault_page_size(Integer.parseInt(prop.getString("default_page_size","20")));
           /* this.setAttach_url(prop.getString("attach_url"));
            this.setHostName(prop.getString("hostName", ""));
            this.setFrom(prop.getString("from", ""));
            this.setFromPwd(prop.getString("fromPwd", ""));
            this.setPromotion_url(prop.getString("promotion_url",""));
            this.setSync_room_isplay_url(prop.getString("sync_room_isplay_url",""));*/
		} catch (ConfigurationException e) {
			 e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
	}

}

